import dorkbox.systemTray.SystemTray;
import dorkbox.systemTray.SystemTrayMenuAction;
import dorkbox.systemTray.swing.SwingSystemTray;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import javax.swing.*;
import java.lang.reflect.Field;
import java.net.URL;

public class TestTrayJavaFX extends Application {

    public static final URL BLACK_MAIL = TestTrayJavaFX.class.getResource("mail.000000.24.png");
    public static final URL GREEN_MAIL = TestTrayJavaFX.class.getResource("mail.39AC39.24.png");
    public static final URL LT_GRAY_MAIL = TestTrayJavaFX.class.getResource("mail.999999.24.png");

    public static
    void main(String[] args) {
        // ONLY if manually loading JNA jars.
        //
        // Not necessary if using the official JNA downloaded from https://github.com/twall/jna AND THAT JAR is on the classpath
        //
//        System.load(new File("../../resources/Dependencies/jna/linux_64/libjna.so").getAbsolutePath()); //64bit linux library

        launch(TestTrayJavaFX.class);
    }

    @Override
    public
    void start(final Stage primaryStage) throws Exception {

        primaryStage.setTitle("Hello World!");
        Button btn = new Button();
        btn.setText("Say 'Hello World'");
        btn.setOnAction(event -> System.out.println("Hello World!"));

        StackPane root = new StackPane();
        root.getChildren().add(btn);
        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.show();


        SystemTray tray = SystemTray.getSystemTray();
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        if (tray == null) {
            throw new RuntimeException("Unable to load SystemTray!");
        }

        tray.setIcon(LT_GRAY_MAIL);

        tray.setStatus("No Mail");

        if (tray instanceof SwingSystemTray) {
            SwingUtilities.invokeLater(() -> {
                try {
                    SwingSystemTray swingTray = (SwingSystemTray) tray;
                    Field menu = SwingSystemTray.class.getDeclaredField("menu");
                    menu.setAccessible(true);
                    Object o = menu.get(swingTray);
                    Field popup_hide_delay = o.getClass().getDeclaredField("POPUP_HIDE_DELAY");
                    popup_hide_delay.setAccessible(true);
                    popup_hide_delay.setLong(o, 250L);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }

        SystemTrayMenuAction callbackGray = (systemTray, menuEntry) -> {
            systemTray.setStatus(null);
            systemTray.setIcon(BLACK_MAIL);

            menuEntry.setCallback(null);
//                systemTray.setStatus("Mail Empty");
            systemTray.removeMenuEntry(menuEntry);
            System.err.println("POW");
        };

        SystemTrayMenuAction callbackGreen = (systemTray, menuEntry) -> {
            systemTray.setIcon(GREEN_MAIL);
            systemTray.setStatus("Some Mail!");

            menuEntry.setCallback(callbackGray);
            menuEntry.setImage(BLACK_MAIL);
            menuEntry.setText("Delete Mail");
//                systemTray.removeMenuEntry(menuEntry);
        };

        tray.addMenuEntry("Green Mail", GREEN_MAIL, callbackGreen);

        tray.addMenuEntry("Quit", (systemTray, menuEntry) -> {
            systemTray.shutdown();
            Platform.exit();  // necessary to close javaFx
            //System.exit(0);  not necessary if all non-daemon threads have stopped.
        });
    }
}
